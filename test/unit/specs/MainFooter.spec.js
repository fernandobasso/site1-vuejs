/* eslint-disable no-unused-expressions */
import Vue from 'vue';
import MainFooter from '@/components/MainFooter';
import router from '@/router';

//
// https://github.com/vuejs-templates/webpack/issues/709
//

describe('MainFooter.vue', () => {
  it('should render correct elements', () => {
    const Ctor = Vue.extend(MainFooter);
    const vm = new Ctor({ router }).$mount();

    // Must contain the current year.
    expect(vm.$el.textContent).to.include((new Date()).getFullYear().toString());
  });
});
