/* eslint-disable no-unused-expressions */
import Vue from 'vue';
import MainHeader from '@/components/MainHeader';
import router from '@/router';

//
// https://github.com/vuejs-templates/webpack/issues/709
//

describe('MainHeader.vue', () => {
  it('should display the logo', () => {
    const Ctor = Vue.extend(MainHeader);
    const vm = new Ctor({ router }).$mount();
    expect(vm.$el.querySelector('.logo img')).to.exist;
  });

  it('should render correct links', () => {
    const Ctor = Vue.extend(MainHeader);
    const vm = new Ctor({ router }).$mount();
    const text = vm.$el.textContent;
    expect(text).to.contain('Sobre');
    expect(text).to.contain('Home');
    expect(text).to.contain('Serviços');
    expect(text).to.contain('Área do Cliente');
    expect(text).to.contain('Contato');
    expect(text).to.contain('CRC/RS N° 3975');
    expect(text).to.contain('54 3313.3755');
  });
});
