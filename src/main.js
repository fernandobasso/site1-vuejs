// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
// import 'font-awesome-webpack';
import App from './App';
import router from './router';

import './scss/bulma-custom.scss';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#vueroot',
  router,
  template: '<App/>',
  components: { App },
});
